## Test project - Spring Boot Test

**Preparation**

Create blank spring project on https://start.spring.io/ , add web, security, jpa, lombok, h2 dependencies from the UI. Use java version 1.8 and maven as a build tool.

It will be required to create git repository on bitbucket or github that will be shared for evaluation.

---

**Task**

Goal of the test is to create service for file sharing between users.

Service endpoints to be implemented:

* POST /register

This endpoint will accept json object with two string properties: email and password. On successful create it will return status 201.
This will be the only public endpoint.

* GET /api/file

This endpoint will return json object with two properties: owned and shared. Both of those will be an array of objects that are representing files owned by authenticated user and files that are shared with him.

* GET /api/file/{id}

This endpoint will be used to download file associated with given ID in uri. User can download file only if he is the owner or file is shared with him else forbidden code should be returned.
For this task assume that all files that users will upload will be plain text files.

* POST /api/file

This endpoint will be used to upload the file and on success it will return file ID that can be used with GET endpoint to download file. 
Make sure that this ID is String.


* POST /api/share

This endpoint will accept json object with two string properties: email and file ID.
It will be used by file owner to share access to the file with other users. Only the file owner can give the access to his file.



Make sure that You are using Hibernate, JPA relational annotations and Spring JPA repositories for managing the data.

Make sure to use spring security to implement basic auth.

Make sure that You are using lombok for generating getters and setters.

Provide service property that will be used to mark directory for uploads. 
“com.demo.uploads.directory” property defined in application.property as default value.
You can overwrite its value using command-prompt param.

**Postman has an official bug - it doesn't set properly content-type during file upload. Please double check it during testing** 

**Security**

All endpoints must be secured with basic auth. Only /register endpoint will be available for public access. Make sure that API returns valid http status code for unauthenticated or unauthorized requests.

**Setup**

Application must be buildable by just “mvn package” command which will create executable jar file in target/ directory.

For evaluation service will be run with “java -jar projectname.jar --com.demo.uploads.directory=/some/dir”
