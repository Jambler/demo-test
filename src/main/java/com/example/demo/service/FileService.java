package com.example.demo.service;

import com.example.demo.model.File;

public interface FileService {

    void save(File file);

    File findById(String id);
}
