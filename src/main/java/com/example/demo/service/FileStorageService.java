package com.example.demo.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileStorageService {

    Resource getFile(String id);

    boolean saveFile(String filename, MultipartFile bytes);
}
