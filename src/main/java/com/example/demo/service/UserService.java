package com.example.demo.service;

import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.User;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    User findById(Integer id);

    User getCurrentUser();

    User findByEmailAndPassword(String email, String password) throws UserNotFoundException;

    User findByEmail(String email);

    Integer save(User user);

    List<User> findAll();
}
