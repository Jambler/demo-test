package com.example.demo.controller.request;

import lombok.Data;

@Data
public class FileSharingRequest {

    private String email;
    private String fileId;
}
