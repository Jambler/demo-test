package com.example.demo.controller;

import com.example.demo.exception.UserAlreadyExistException;
import com.example.demo.model.User;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("")
public class UserController {

    private UserService userService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserController(UserService userService, PasswordEncoder passwordEncoder) {
        this.userService = userService;
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/register")
    public ResponseEntity registerUser(@RequestBody @Valid User user, UriComponentsBuilder uriBuilder) throws UserAlreadyExistException {
        User userByEmail = userService.findByEmail(user.getEmail());
        if (userByEmail != null)
            return ResponseEntity.badRequest().body("Such email already registered in the system. Try to login.");
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        userService.save(user);
        UriComponents uriComponents =
                uriBuilder.path("/users/{id}").buildAndExpand(user.getId());
        return ResponseEntity.created(uriComponents.toUri()).build();
    }

    @GetMapping(value = "/users/")
    public ResponseEntity<List<User>> getUsersList() {
        return ResponseEntity.ok(userService.findAll());
    }
}
