package com.example.demo.controller;

import com.example.demo.model.File;
import com.example.demo.model.User;
import com.example.demo.model.UserFilesDto;
import com.example.demo.service.FileService;
import com.example.demo.service.FileStorageService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.UUID;

@RestController
@RequestMapping("/api/file")
public class FileController {

    private final UserService userService;
    private final FileService fileService;
    private final FileStorageService fileStorageService;

    @Autowired
    public FileController(UserService userService,
                          FileService fileService,
                          FileStorageService fileStorageService) {
        this.userService = userService;
        this.fileService = fileService;
        this.fileStorageService = fileStorageService;
    }

    @GetMapping("/")
    public ResponseEntity<UserFilesDto> getFiles() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User byId = userService.findById(((User) auth.getPrincipal()).getId());
        return ResponseEntity.ok(new UserFilesDto(byId.getFiles(), byId.getSharedFiles()));
    }

    @PostMapping("/")
    public ResponseEntity uploadFile(@RequestParam("file") MultipartFile file) {
        if (file.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }

        User user = userService.getCurrentUser();
        File userFile = new File();
        userFile.setUser(user);
        userFile.setId(UUID.randomUUID().toString() + UUID.randomUUID().toString());
        userFile.setFileName(userFile.getId() + file.getOriginalFilename());

        if (!fileStorageService.saveFile(userFile.getFileName(), file))
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Something goes wrong. Try later.");

        fileService.save(userFile);
        user.getFiles().add(userFile);
        userService.save(user);

        return ResponseEntity.ok(userFile.getId());
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String id) {
        User user = userService.getCurrentUser();
        File fileById = fileService.findById(id);
        if (!fileById.getUser().equals(user) && !user.getSharedFiles().contains(fileById)) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).build();
        }
        if (!fileById.getUser().getId().equals(user.getId()))
            return ResponseEntity.notFound().build();

        Resource resource = fileStorageService.getFile(fileById.getFileName());

        if (resource == null)
            return ResponseEntity.notFound().build();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(MediaType.APPLICATION_OCTET_STREAM_VALUE))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

}
