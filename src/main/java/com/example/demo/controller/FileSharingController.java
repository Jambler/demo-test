package com.example.demo.controller;

import com.example.demo.controller.request.FileSharingRequest;
import com.example.demo.model.File;
import com.example.demo.model.User;
import com.example.demo.service.FileService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/share")
public class FileSharingController {

    private final UserService userService;

    private final FileService fileService;

    @Autowired
    public FileSharingController(UserService userService, FileService fileService) {
        this.userService = userService;
        this.fileService = fileService;
    }

    @PostMapping("/")
    public ResponseEntity getFiles(@RequestBody FileSharingRequest request){
        User user = userService.getCurrentUser();
        if (request.getEmail().equals(user.getEmail())) return ResponseEntity.badRequest().body("Target user is a file owner.");
        User shareToUser = userService.findByEmail(request.getEmail());
        if (shareToUser == null) return ResponseEntity.badRequest().body("No such user");
        File file = fileService.findById(request.getFileId());
        if (file == null) return ResponseEntity.badRequest().body("No such file");
        if (!file.getUser().equals(user)) return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Operation denied");
        if (shareToUser.getSharedFiles().contains(file)) return ResponseEntity.badRequest().body("File already shared");
        file.getUsersShared().add(shareToUser);
        shareToUser.getSharedFiles().add(file);
        userService.save(shareToUser);
        //fileService.save(file);
        return ResponseEntity.ok().build();
    }
}
