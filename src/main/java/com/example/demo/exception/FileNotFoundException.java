package com.example.demo.exception;

public class FileNotFoundException extends ApiException {

    private String message;

    public FileNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
