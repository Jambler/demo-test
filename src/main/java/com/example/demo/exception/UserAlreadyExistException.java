package com.example.demo.exception;

public class    UserAlreadyExistException extends ApiException {

    private String message;

    public UserAlreadyExistException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
