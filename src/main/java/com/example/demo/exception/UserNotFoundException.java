package com.example.demo.exception;

public class UserNotFoundException extends ApiException {

    private String message;

    public UserNotFoundException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
